import styled, { keyframes } from "styled-components";
import LoginImage from "../../assets/img/runner-login-desktop.svg";
import Wave from "../../assets/img/wave-login-desktop.svg";
import Sun from "../../assets/img/sun-login-desktop.svg";

const appear = keyframes`
from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

export const Container = styled.div`
  animation: ${appear} 0.4s;
  height: 100vh;
  display: flex;
  align-items: stretch;
  background-color: var(--background-color);
  .wave {
    background: url(${Wave}) no-repeat;
    background-size: cover;
    width: 100vw;
    height: 35vh;
    position: fixed;
    bottom: 0px;
    left: 0;
    @media (min-width: 768px) {
      height: 25vh;
    }
  }
  @media (min-width: 768px) {
    .sun {
      background: url(${Sun}) no-repeat center;
      background-size: contain;
      width: 100vw;
      height: 10vh;
      position: fixed;
      top: 0px;
      left: 50%;
      transform: translateX(-50%);
    }
  }
`;

export const Background = styled.div`
  @media (min-width: 768px) {
    flex: 1;
    background: url(${LoginImage}) no-repeat center;
  }
`;

export const FormContent = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  width: 100vw;
  justify-content: flex-start;
  align-items: center;
  text-align: center;
  @media (min-width: 768px) {
    justify-content: center;
    align-self: center;
    justify-self: center;
    flex: 1;
  }

  form {
    height: 40%;
    width: 70%;
    display: flex;
    flex-direction: column;
    align-self: center;
    justify-content: space-between;
    align-items: center;
    margin-top: 50px;
    @media (min-width: 1100px) {
      width: 50%;
      height: 30%;
      margin-top: 0;
    }
  }

  h1 {
    padding: 20px 0px;
    width: 100vw;
    background-color: var(--purple);
    justify-self: flex-start;
    text-align: center;
    font-size: 36px;
    font-family: var(--font-title);
    color: white;
    @media (min-width: 768px) {
      width: 50%;
      background-color: transparent;
      color: var(--purple);
      font-size: 44px;
    }
  }

  .register {
    color: var(--purple);
    font-weight: bold;
    text-decoration: none;
  }
`;
