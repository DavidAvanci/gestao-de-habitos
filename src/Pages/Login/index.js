//import Form
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

// Import Components
import { Button } from "../../components/Button";
import { Input } from "../../components/Input";

// Import API baseUrl
import api from "../../services/api";

// Import react-router-dom
import { useHistory } from "react-router";
import { Link, Redirect } from "react-router-dom";

// Import JWT-decode
import jwt_decode from "jwt-decode";

// Import Toast
import { toast } from "react-toastify";

// Import Styled components
import { Container, Background, FormContent } from "./style";
import { useContext, useEffect } from "react";
import { HabitsContext } from "../../Providers/Habits";
import { GroupsContext } from "../../Providers/Groups";
import { UserContext } from "../../Providers/User";

export const Login = ({ authenticated, setAuthenticated }) => {
  const history = useHistory();

  const { setHabitsList, setToken } = useContext(HabitsContext);
  const { setSubscriptions, setTokenGroups } = useContext(GroupsContext);

  const schema = yup.object().shape({
    username: yup.string().required("Campo obrigatório"),
    password: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (data) => {
    api
      .post("sessions/", data)
      .then((resp) => {
        const { access } = resp.data;
        const decoded = jwt_decode(access);
        const { user_id } = decoded;
        localStorage.setItem("@GH:token", JSON.stringify(access));
        localStorage.setItem("@GH:userId", JSON.stringify(user_id));
        setToken(JSON.parse(localStorage.getItem("@GH:token")));
        setTokenGroups(JSON.parse(localStorage.getItem("@GH:token")));
        setHabitsList(undefined);
        setSubscriptions(undefined);
        setAuthenticated(true);
        return history.push("/dashboard/habits");
      })
      .catch((err) => {
        console.log(err);
        toast.error("Usuário ou senha inválidos");
      });
  };

  if (authenticated) {
    return <Redirect to="/dashboard/habits" />;
  }

  return (
    <Container>
      <div className="sun" />
      <FormContent>
        <h1>Login</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            name="username"
            register={register}
            placeholder="Usuário"
            isError={errors.username}
          />
          <Input
            name="password"
            register={register}
            type="password"
            placeholder="Senha"
            isError={errors.password}
          />
          <Button type="submit">Entrar</Button>
          <span>
            Ainda não possui conta?{" "}
            <Link className="register" to="/register">
              Cadastre-se
            </Link>
          </span>
        </form>
      </FormContent>
      <Background />
      <div className="wave" />
    </Container>
  );
};
