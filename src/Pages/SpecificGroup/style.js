import styled, { keyframes } from "styled-components";

const appear = keyframes`
from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

export const SmoothingAnimation = styled.div`
  animation: ${appear} 0.4s;
`;

export const Container = styled.div`
  position: sticky;
  top: 0;
  background-color: var(--background-color);
  .groupName {
    text-align: center;
    font-size: 28px;
    color: var(--purple);
    font-family: var(--font-title);
  }
  @media (min-width: 768px) {
    .groupName {
      font-size: 35px;
    }
    .header {
      display: none;
    }
  }
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  align-content: center;
  align-items: center;

  margin: 20px 0px;

  .buttonActivity {
    border-bottom: ${(props) =>
      props.isActivity ? "4px solid var(--purple)" : "4px solid var(--pink)"};
    width: 45%;
    padding-right: 0px 40px;
    font-size: 25px;
    color: var(--purple);
    font-weight: bold;
    @media (min-width: 768px) {
      width: 25%;
    }
  }
  .buttonGoal {
    border-bottom: ${(props) =>
      props.isActivity ? "4px solid var(--pink)" : "4px solid var(--purple)"};
    width: 45%;
    padding: 0px 40px;
    font-size: 25px;
    color: var(--purple);
    font-weight: bold;
    @media (min-width: 768px) {
      width: 25%;
    }
  }
`;
