import { useEffect, useState } from "react";
import { ButtonContainer, Container, SmoothingAnimation } from "./style";
import { Header } from "../../components/Header";
import { Activities } from "../Activities";
import { Goals } from "../Goals";
import { PreviousPage } from "../../components/PreviousPage";
import { useHistory, useParams } from "react-router-dom";
import api from "../../services/api";
import { Redirect } from "react-router-dom";
import { HeaderDesktop } from "../../components/HeaderDesktop";

export const SpecificGroup = ({ authenticated, setAuthenticated }) => {
  const [isActivity, setIsActivity] = useState(true);
  const [groupName, setGroupName] = useState("");
  const [visible, setVisible] = useState("none");

  let { id } = useParams();

  const getSpecificGroup = () => {
    api.get(`groups/${id}/`).then((res) => setGroupName(res.data.name));
  };

  useEffect(() => {
    getSpecificGroup();
  }, []);

  const history = useHistory();

  const previousPage = () => {
    history.push("/dashboard/groups");
  };

  if (!authenticated) {
    return <Redirect to="/login" />;
  }

  return (
    <SmoothingAnimation>
      <Container>
        <div className="header">
          <Header
            isUser={true}
            setVisible={setVisible}
            visible={visible}
            setAuthenticated={setAuthenticated}
          />
        </div>
        <HeaderDesktop
          setVisible={setVisible}
          visible={visible}
          setAuthenticated={setAuthenticated}
        />
        <PreviousPage previousPage={previousPage} />
        <h1 className="groupName">{groupName}</h1>
        <ButtonContainer isActivity={isActivity}>
          <button
            className="buttonActivity"
            onClick={() => setIsActivity(true)}
          >
            {" "}
            Atividades
          </button>
          <button className="buttonGoal" onClick={() => setIsActivity(false)}>
            {" "}
            Objetivos{" "}
          </button>
        </ButtonContainer>
      </Container>
      {isActivity ? <Activities /> : <Goals />}
    </SmoothingAnimation>
  );
};
