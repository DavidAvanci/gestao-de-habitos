import { Redirect, useHistory } from "react-router-dom";
import { Button } from "../../components/Button";
import { Header } from "../../components/Header";
import { Container, LinksContent } from "./styles";
import { Logo } from "../../components/Logo";
import imgHome from "../../assets/img/mainhp.png";
import ellipse from '../../assets/img/Ellipse 11.png';

export const Home = ({ authenticated }) => {
  const history = useHistory();
  if (authenticated) {
    return <Redirect to="/dashboard/habits" />;
  }

  return (
    <Container>
      <div className="headerHome">
        <Header isUser={false}>
          <Logo />
        </Header>
      </div>
      <div className="imgHome">
        <p className="title">Fit++</p>
        <img src={imgHome} alt="" />
      </div>
      <span className="ellipse"><img src={ellipse} alt="" /></span>
      <LinksContent>
        <span>Incremente sua saúde</span>
        <div className="line"></div>
        <div className="buttons">
          <Button className="login" onClick={() => history.push("/login")}>
            Login
          </Button>
          <Button
            className="register"
            onClick={() => history.push("/register")}
          >
            Cadastre-se
          </Button>
        </div>
      </LinksContent>
      <div className="wave"></div>
    </Container>
  );
};
