import styled, { keyframes } from "styled-components";
import Sun from "../../assets/img/sun-login-desktop.svg";
import Wave from "../../assets/img/wave-login-desktop.svg";

const appear = keyframes`
from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

export const Container = styled.div`
  animation: ${appear} 0.4s;
  background-color: var(--background-color);
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;

  .headerHome {
    width: 100%;
    @media (min-width: 768px) {
      display: none;
    }
  }

  .title {
    @media (min-width: 768px) {
      width: 50px;
      font-size: 36px;
      font-family: var(--font-title);
      color: var(--purple);
    }
  }

  .wave {
    background: url(${Wave}) no-repeat;
    background-size: cover;
    width: 100vw;
    height: 35vh;
    position: fixed;
    bottom: 0px;
    left: 0;
    @media (min-width: 768px) {
      height: 25vh;
    }
  }

  @media (min-width: 768px) {
    .sun {
      background: url(${Sun}) no-repeat center;
      background-size: contain;
      width: 100vw;
      height: 10vh;
      position: fixed;
      top: 0px;
      left: 50%;
      transform: translateX(-50%);
    }
  }

  .imgHome {
    display: none;
    @media (min-width: 768px) {
      display: flex;
      flex-direction: column;
      justify-content: center;
      width: 100%;
      align-items: center;

      p {
        position: absolute;
        top: 60px;
      }

      img {
        width: 90%;
      }
    }
  }

  .ellipse {
    display: none;
    @media (min-width: 768px) {
      display: block;
      position: absolute;
      margin-left: auto;
      margin-right: auto;
      left: 0;
      right: 0;
      width: 100%;
      text-align: center;
    }
  }

  @media (min-width: 768px) {
    flex-direction: row;
    align-items: stretch;
  }
`;

export const LinksContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
  .line {
    width: 80%;
    border: 0.5px solid var(--purple);
    margin-bottom: 50px;
  }

  .login {
    margin-bottom: 30px;
  }

  span {
    margin-top: 30px;
    margin-bottom: 30px;
    width: 40%;
    text-align: center;
    font-family: var(--font-text);
    color: var(--purple);
  }
  @media (min-width: 768px) {
    flex-direction: column;
    justify-content: center;
    .line {
      display: none;
    }
    span {
      width: 100%;
      font-size: 25px;
      margin-bottom: 200px;
    }
    .buttons {
      display: flex;
      flex-direction: row;
      margin-right: 80px;
      margin-bottom: 40px;

      button {
        width: 180px;
        margin-left: 60px;
      }
    }
  }
`;
