import { useEffect, useState } from "react";
import { useContext } from "react";
import { useParams } from "react-router";
import { ActivitiesContext } from "../../Providers/Activities";
import { ActivityCard } from "../../components/ActivityCard";
import { CardsContaier, Container } from "./style";
import { Button } from "../../components/Button";
import { Header } from "../../components/Header";
import { ModalActivitiesEdit } from "../../components/ModalActivitiesEdit";
import { ModalActivityCreate } from "../../components/ModalActivityCreate";
import { CircularProgress } from "@material-ui/core";

export const Activities = () => {
  let { id } = useParams();

  const { getGroupActivities, groupActivities, deleteActivity } =
    useContext(ActivitiesContext);

  const [modalEditActivities, setModalEditActivities] = useState("none");
  const [modalNewActivity, setModalNewActivity] = useState("none");
  const [activities, setActivities] = useState("");

  const handleEdit = (activities) => {
    setActivities(activities);
    setModalEditActivities("flex");
  };

  useEffect(() => {
    getGroupActivities(id);
  }, []);

  return (
    <Container>
      <Button
        className="buttonActivity"
        onClick={() => setModalNewActivity("flex")}
      >
        Criar Nova
      </Button>
      <CardsContaier>
        {groupActivities === undefined ? (
          <CircularProgress />
        ) : groupActivities.length === 0 ? (
          <div>Esse grupo ainda não possui atividades</div>
        ) : (
          groupActivities.map((activities, idx) => (
            <ActivityCard
              key={idx}
              pencilFunction={handleEdit}
              group={activities}
              isEditable={true}
              deleteFunction={deleteActivity}
              id={activities.id}
              groupId={id}
              activitiesTitle={activities.title}
              date={activities.realization_time}
            ></ActivityCard>
          ))
        )}
      </CardsContaier>
      <ModalActivitiesEdit
        modalEditActivities={modalEditActivities}
        setModalEditActivities={setModalEditActivities}
        activities={activities}
      />
      <ModalActivityCreate
        modalNewActivity={modalNewActivity}
        setModalNewActivity={setModalNewActivity}
        id={id}
        setActivities={setActivities}
      />
      <div className="wave" />
    </Container>
  );
};
