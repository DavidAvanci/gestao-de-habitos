import styled, { css } from "styled-components";

export const ModalContainer = styled.div`
  background-color: var(--light-blue);
  border-radius: 8px;
  width: 92%;
  position: absolute;
  top: 70px;
  left: 50%;
  transform: translateX(-50%);
  padding: 0 10px 20px;
  display: ${(props) => props.modalVisible};

  .searchBar {
    display: flex;
    justify-content: center;
  }

  .searchBar button {
    width: 50px;
    font-size: 24px;
    margin-left: 12px;
  }

  .closeModal {
    color: var(--purple);
    border: 1px solid var(--purple);
    font-size: 15px;
    font-family: sans-serif;
    border-radius: 50%;
    width: 28px;
    padding: 6px;
  }
  .stickyComponents {
    background-color: var(--off-white);
    position: sticky;
    top: 0px;
    padding: 16px;
    border-radius: 6px;
    width: 90%;
    max-width: 280px;
  }

  .modal {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  div {
    margin: 10px 0;
  }

  input {
    width: 90%;
  }

  .pageSetter {
    margin: 0;
    display: flex;
    flex-direction: column;
    align-items: center;

    button {
      font-size: 12px;
      width: 26px;
      margin: 10px;
    }
  }

  @media (min-width: 768px) {
    .stickyComponents {
      width: 90%;
      max-width: 550px;
    }

    .pageSetter {
      flex-direction: row;
      justify-content: space-around;

      button {
        font-size: 20px;
      }
    }
  }
`;

export const GroupsAvailable = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;

  div {
    margin: 10px;
  }
`;

export const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 10px;
  width: 100%;

  button {
    margin-top: 10px;
    width: 60%;
    font-size: 14px;
    margin: 8px;
    color: var(--purple);
    font-family: var(--font-text);
  }

  @media (min-width: 768px) {
    flex-direction: row;
    justify-content: center;
    margin-top: 40px;
  }
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-content: start;
  margin-top: 10px;

  h1 {
    color: var(--purple);
    font-family: var(--font-title);
    font-weight: 500;
    text-transform: uppercase;
  }

  @media (min-width: 768px) {
    flex-direction: row;

    h1 {
      margin-top: 20px;
    }
  }
`;

export const Container = styled.div`
  display: flex;
  justify-content: center;
`;

export const MyGroups = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: 768px) {
    flex-direction: row;
    justify-content: center;
    flex-wrap: wrap;
  }
`;

export const CreatedGroups = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: 768px) {
    margin-top: 0;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
  }
`;

export const GroupsContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  text-align: center;
  .infosCard {
    text-align: center;
    width: ${({ isEditable }) => (isEditable ? "150px" : "200px")};
    :hover {
      color: var(--purple);
      text-shadow: 2px 2px 4px lightgray;
    }
    h2 {
      font-size: 1.1rem;
      color: var(--purple);
      font-weight: bold;
    }
    .descriptionCard {
      color: gray;
      font-size: 13px;
    }
    .categoryCard {
      text-decoration: underline;
      font-weight: lighter;
    }
  }
`;
