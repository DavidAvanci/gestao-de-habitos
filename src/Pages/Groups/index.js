import { useContext, useEffect, useState } from "react";
import {
  ModalContainer,
  GroupsAvailable,
  Content,
  Container,
  CreatedGroups,
  ButtonsContainer,
  GroupsContainer,
  MyGroups,
} from "./style";
import { useHistory } from "react-router-dom";
import { GroupsContext } from "../../Providers/Groups";

import { Button } from "../../components/Button";
import { Input } from "../../components/Input";
import { GroupCard } from "../../components/GroupCard";
import { GroupAddCard } from "../../components/GroupAddCard";
import { NewGroup } from "../../components/ModalGroupCreate";
import { EditGroup } from "../../components/ModalGroupEdit";

import { CircularProgress } from "@material-ui/core";

export const Groups = () => {
  const [modalVisible, setModalVisible] = useState("none");
  const [modalNewGroup, setModalNewGroup] = useState("none");
  const [modalEditGroup, setModalEditGroup] = useState("none");
  const [searchGroup, setSearchGroup] = useState("");
  const [userId] = useState(JSON.parse(localStorage.getItem("@GH:userId")));
  const [group, setGroup] = useState("");
  const [page, setPage] = useState(1);

  const history = useHistory();

  const gambiarra = () => {};

  const {
    error,
    subscriptions,
    allGroups,
    specificGroup,
    isLoading,
    pages,
    setIsLoading,
    subscribeToGroup,
    getSubscriptions,
    getAllGroups,
    getSpecificGroup,
    setSpecificGroup,
  } = useContext(GroupsContext);

  const handleCard = (id) => {
    history.push(`/groups/${id}`);
  };

  const handleClick = (group) => {
    subscribeToGroup(group);
  };

  const handleEdit = (group) => {
    console.log(group);
    setModalEditGroup("block");
    setGroup(group);
  };

  const handlePrev = () => {
    if (page - 1 > 0) {
      setPage(page - 1);
    }
  };
  const handleNext = () => {
    if (page + 1 < pages) {
      setPage(page + 1);
    }
  };

  useEffect(() => {
    getAllGroups(page);
    getSubscriptions();
  }, [page]);

  useEffect(() => {
    setSpecificGroup("");
    getSpecificGroup(searchGroup);
  }, [searchGroup]);

  return (
    <>
      <ButtonsContainer>
        <Button onClick={() => setModalNewGroup("block")}>Novo grupo</Button>
        <Button
          onClick={() => {
            setModalVisible("block");
          }}
        >
          Pesquisar grupo
        </Button>
      </ButtonsContainer>
      <Container>
        {subscriptions === undefined ? (
          <CircularProgress />
        ) : subscriptions.length === 0 ? (
          <div>Você ainda não possui grupos</div>
        ) : (
          <Content>
            <GroupsContainer>
              <h1>Minhas Inscrições</h1>
              <MyGroups>
                {subscriptions
                  .filter((group) => group.creator.id !== userId)
                  .map((sub, index) => (
                    <GroupCard isEditable={false} key={index}>
                      <button onClick={(e) => handleCard(sub.id)}>
                        <div className="infosCard">
                          <h2>{sub.name}</h2>
                          <p className="descriptionCard">
                            {sub.description.slice(0, 15)}{" "}
                            {!!sub.description[15] ? "..." : ""}
                          </p>
                          <span className="categoryCard">
                            Categoria:
                            {sub.category.slice(0, 10)}{" "}
                            {!!sub.category[15] ? "..." : ""}
                          </span>
                        </div>
                      </button>
                    </GroupCard>
                  ))}
              </MyGroups>
            </GroupsContainer>
            <GroupsContainer isEditable>
              <h1>Grupos criados pelo usuário</h1>
              <CreatedGroups>
                {subscriptions
                  .filter((group) => group.creator.id === userId)
                  .map((sub, index) => (
                    <GroupCard
                      isLoading={isLoading}
                      isEditable={true}
                      pencilFunction={handleEdit}
                      group={sub}
                      key={index}
                    >
                      <button onClick={(e) => handleCard(sub.id)}>
                        <div className="infosCard">
                          <h2>{sub.name}</h2>
                          <p className="descriptionCard">
                            {sub.description.slice(0, 25)}{" "}
                            {!!sub.description[25] ? "..." : ""}
                          </p>
                          <span className="categoryCard">
                            Categoria: {sub.category.slice(0, 10)}{" "}
                            {!!sub.category[15] ? "..." : ""}
                          </span>
                        </div>
                      </button>
                    </GroupCard>
                  ))}
              </CreatedGroups>
            </GroupsContainer>
          </Content>
        )}
      </Container>
      <ModalContainer modalVisible={modalVisible}>
        <div className="modal">
          <div className="stickyComponents">
            <button
              className="closeModal"
              onClick={() => {
                setSpecificGroup("");
                setSearchGroup("");
                setModalVisible("none");
              }}
            >
              X
            </button>
            <div className="searchBar">
              <Input
                register={gambiarra}
                value={searchGroup}
                onChange={(e) => setSearchGroup(e.target.value)}
              />
              <Button
                onClick={() => {
                  getSpecificGroup(searchGroup);
                  setSearchGroup("");
                }}
              >
                <i className="fas fa-search"></i>
              </Button>
            </div>
            <div className="pageSetter">
              <button onClick={handlePrev}>
                <i class="fas fa-arrow-left"></i>
              </button>
              <span>Página atual: {page}</span>
              <button onClick={handleNext}>
                <i class="fas fa-arrow-right"></i>
              </button>
            </div>
          </div>
          <GroupsAvailable>
            {searchGroup === "" ? (
              allGroups.map((group, idx) => (
                <GroupAddCard
                  key={idx}
                  pencilFunction={handleClick}
                  group={group}
                  isEditable
                >
                  {group.name}
                </GroupAddCard>
              ))
            ) : !error && specificGroup.length > 0 ? (
              specificGroup.map((group, idx) => (
                <GroupAddCard
                  key={idx}
                  pencilFunction={handleClick}
                  group={group}
                  isEditable
                >
                  {group.name}
                </GroupAddCard>
              ))
            ) : (
              <div>Nenhum grupo com este nome</div>
            )}
          </GroupsAvailable>
        </div>
      </ModalContainer>
      <NewGroup
        setModalNewGroup={setModalNewGroup}
        userId={userId}
        modalNewGroup={modalNewGroup}
      />
      <EditGroup
        group={group}
        setModalEditGroup={setModalEditGroup}
        setIsLoading={setIsLoading}
        modalEditGroup={modalEditGroup}
      />
    </>
  );
};
