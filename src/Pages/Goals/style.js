import styled from "styled-components";
import Wave from "../../assets/img/wave-login-desktop.svg";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-content: center;
  align-items: center;

  .buttonGoal {
    width: 90%;
    margin-bottom: 20px;
  }
  .wave {
    background: url(${Wave}) no-repeat;
    background-size: cover;
    width: 100vw;
    height: 35vh;
    position: fixed;
    bottom: 0px;
    left: 0;
    z-index: -1;
  }
`;

export const CardsContaier = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  align-content: center;
`;
