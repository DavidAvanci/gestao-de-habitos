// Impor from React
import { useEffect, useState, useContext } from "react";

// Import from React Router
import { useParams } from "react-router";

// Import Context -- GOALS
import { GoalsContext } from "../../Providers/Goals";

// Import Components
import { Button } from "../../components/Button";
import { GoalsCard } from "../../components/GoalsCard";
import { ModalGoalCreate } from "../../components/ModalGoalCreate";
import { ModalGoalEdit } from "../../components/ModalGoalEdit";
import { Container, CardsContaier } from "./style";
import { CircularProgress } from "@material-ui/core";

export const Goals = () => {
  let { id } = useParams(); // Parametro que fornece o id do grupo

  // Funções fornecidas pelo context
  const { groupGoals, getGroupGoals, deleteGoal } = useContext(GoalsContext);

  // State declarado para controlar a vistibilidade do modal de criação de novo Goal;
  const [modalNewGoal, setModalNewGoal] = useState("none");

  // State responsável por capturar objetivo a ser editado pelo usuário;
  const [goal, setGoal] = useState("");

  // State  declarado para controlar visibilidade do modal de edição
  const [modalEditGoals, setModalEditGoals] = useState("none");

  // Função acionanda quando o 'Pencil' de edição é clicado;
  const handleEdit = (goal) => {
    setGoal(goal); // captura o goal que será editado
    setModalEditGoals("flex"); // Torna o modal visível
  };

  // responsável por carregar os objetivos presentes dentro do grupo
  useEffect(() => {
    getGroupGoals(id);
  }, []);

  return (
    <Container>
      <Button className="buttonGoal" onClick={() => setModalNewGoal("flex")}>
        Criar Novo
      </Button>
      <CardsContaier>
        {groupGoals === undefined ? (
          <CircularProgress />
        ) : groupGoals.length === 0 ? (
          <div>Esse grupo ainda não possui objetivos</div>
        ) : (
          groupGoals.map((goals, idx) => (
            <GoalsCard
              key={idx}
              pencilFunction={handleEdit}
              group={goals}
              isEditable={true}
              deleteFunction={deleteGoal}
              id={goals.id}
              groupId={id}
              progress={goals.how_much_achieved}
              goalTitle={goals.title}
            >
              <p>Objetivo: {goals.title}</p>
              <p>Progresso: {goals.how_much_achieved}%</p>
            </GoalsCard>
          ))
        )}
      </CardsContaier>
      <ModalGoalCreate
        modalNewGoal={modalNewGoal}
        setModalNewGoal={setModalNewGoal}
      />
      <ModalGoalEdit
        modalEditGoals={modalEditGoals}
        setModalEditGoals={setModalEditGoals}
        goal={goal}
      />
      <div className="wave" />
    </Container>
  );
};
