//import Form
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

// Import Components
import { Button } from "../../components/Button";
import { Input } from "../../components/Input";
import { Header } from "../../components/Header";
import { Container } from "./style";
import waveRegisterMobile from "../../assets/img/wave_register_mobile.png";
import runnerInRegister from "../../assets/img/runner_in_register.svg";

// Import API baseUrl
import api from "../../services/api";

// Import react-router-dom
import { useHistory, Link, Redirect } from "react-router-dom";

// Import Toast
import { toast } from "react-toastify";

export const Register = ({ authenticated }) => {
  const history = useHistory();
  const formSchema = yup.object().shape({
    username: yup
      .string()
      .required("Nome de usuário obrigatório")
      .max(13, "Máximo 13 caracteres"),
    email: yup
      .string()
      .required("E-mail obrigatório")
      .matches(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "E-mail inválido"
      ),
    password: yup
      .string()
      .required("Senha obrigatória")
      .min(6, "Mínimo 6 caracteres"),
    rePass: yup
      .string()
      .required("Confirmação obrigatória")
      .oneOf([yup.ref("password")], "Senha diferente"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const onSubmit = ({ username, email, password }) => {
    api
      .post("/users/", { username, email, password })
      .then((_) => history.push("/login"))
      .catch((_) => toast.error("Erro"));
  };

  if (authenticated) {
    return <Redirect to="/dashboard/habits" />;
  }

  return (
    <Container>
      <div className="blueBoll" />
      <div className="headerRegister">
        <Header isUser={false}>
          <p>Cadastro</p>
        </Header>
      </div>
      <div className="leftInDesktop">
        <h1 className="titleRegister">Cadastro</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            placeholder="Nome de usuário"
            name="username"
            register={register}
            isError={errors.username}
          />
          <Input
            placeholder="Email"
            name="email"
            register={register}
            isError={errors.email}
          />
          <Input
            placeholder="Senha"
            type="password"
            name="password"
            register={register}
            isError={errors.password}
          />
          <Input
            type="password"
            name="rePass"
            register={register}
            isError={errors.rePass}
          />
          <Button type="submit">CADASTRAR</Button>
        </form>
        <p className="toLogin">
          Já possui conta? Faça seu <Link to="/login">Login</Link>
        </p>
      </div>
      <div className="rightInDesktop">
        <img src={runnerInRegister} alt="" />
      </div>
      <img className="waveRegisterMobile" src={waveRegisterMobile} alt="" />
    </Container>
  );
};
