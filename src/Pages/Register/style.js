import styled, { keyframes } from "styled-components";

const pageApear = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

export const Container = styled.article`
  background-color: var(--background-color);
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  animation: ${pageApear} 0.4s linear;
  @media (min-width: 768px) {
    flex-direction: row;
  }

  .headerRegister {
    width: 100%;
    @media (min-width: 768px) {
      display: none;
    }
  }

  .leftInDesktop {
    z-index: 1;
    text-align: center;
    width: 100%;
    @media (min-width: 768px) {
      width: 50%;
    }
  }

  .titleRegister {
    display: none;
    @media (min-width: 768px) {
      display: initial;
      font-size: 36px;
      font-family: var(--font-title);
      color: var(--purple);
      width: 100%;
    }
  }

  form {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    height: 80%;
    max-height: 500px;
    width: 70%;
    margin: 10vh auto 0;
    z-index: 1;
    @media (min-width: 768px) {
      margin: 1rem auto 0;
      height: 350px;
      width: 50%;
    }
  }
  .toLogin {
    font-family: var(--font-text);
    margin: 2rem auto;
    z-index: 1;
  }

  .toLogin a {
    color: var(--purple);
    font-weight: bold;
  }

  .waveRegisterMobile {
    position: fixed;
    bottom: 0;
    z-index: 0;
    height: 25vh;
    width: 100%;
  }
  .rightInDesktop {
    display: none;
    @media (min-width: 768px) {
      display: flex;
      justify-content: flex-start;
      align-items: center;
      width: 50%;
      overflow: hidden;
    }
  }

  .blueBoll {
    display: none;
    @media (min-width: 768px) {
      display: initial;
      position: absolute;
      top: 0;
      left: 50%;
      transform: translate(-50%, -50%);

      width: 100px;
      height: 100px;
      background-color: var(--light-blue);
      border-radius: 100%;
    }
  }
`;
