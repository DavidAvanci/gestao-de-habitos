import { useState } from "react";
import { ButtonContainer, Container, SmoothingAnimation } from "./styles";
import { Header } from "../../components/Header";
import { Groups } from "../Groups";
import { Redirect, useHistory, useParams } from "react-router-dom";
import { HeaderDesktop } from "../../components/HeaderDesktop";
import { Habits } from "../../components/Habits";

export const Dashboard = ({ authenticated, setAuthenticated }) => {
  const [visible, setVisible] = useState("none");
  const history = useHistory();
  let { toGo } = useParams();

  if (!authenticated) {
    return <Redirect to="/login" />;
  }

  const changeUrl = (addUrl) => {
    toGo = addUrl;
    history.push(`/dashboard/${toGo}`);
  };
  
  return (
    <SmoothingAnimation>
      <Container>
        <div className="header">
          <Header
            isUser={true}
            setVisible={setVisible}
            visible={visible}
            setAuthenticated={setAuthenticated}
          />
        </div>
        <HeaderDesktop
          setVisible={setVisible}
          visible={visible}
          setAuthenticated={setAuthenticated}
        />
        <h1 className="groupName">DashBoard</h1>
        <ButtonContainer isHabit={toGo}>
          <button className="buttonHabits" onClick={() => changeUrl("habits")}>
            {" "}
            Hábitos
          </button>
          <button className="buttonGroups" onClick={() => changeUrl("groups")}>
            {" "}
            Grupos{" "}
          </button>
        </ButtonContainer>
      </Container>
      {toGo === 'habits' ? <Habits /> : <Groups />}
    </SmoothingAnimation>
  );
};
