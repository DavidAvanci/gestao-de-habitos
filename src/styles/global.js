import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  *{
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	font-family: var(--font-text);
	vertical-align: baseline;
	text-decoration: none;
	background: 100vh;
	
}

	:root{
		--font-title: 'Sriracha', cursive;
		--font-text: 'Nunito', sans-serif;
		--pink: #F2C2EF;
		--background-color: #F2DFDF;
		--purple: #6f4bf2;
		--light-blue: #07B0F2;
		--yellow: #F2AE30;
		--orange: #FE7D53;
		--off-white: rgba(255, 255, 255, 0.75);
	}

	body {
	  background-color: #F2DFDF;
  	}

	button {
	  cursor: pointer;
  	}
`;
