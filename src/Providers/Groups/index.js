import { createContext, useState } from "react";
import { toast } from "react-toastify";
import api from "../../services/api";

export const GroupsContext = createContext();

export const GroupsProvider = ({ children }) => {
  const [subscriptions, setSubscriptions] = useState(undefined);
  const [allGroups, setAllGroups] = useState([]);
  const [specificGroup, setSpecificGroup] = useState("");
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [pages, setPages] = useState("");

  const [tokenGroups, setTokenGroups] = useState(
    JSON.parse(localStorage.getItem("@GH:token"))
  );

  api.get("groups/").then((res) => {
    const nOfPages = res.data.count / 15;
    setPages(Math.ceil(nOfPages));
  });

  const getSubscriptions = () => {
    api
      .get("groups/subscriptions/", {
        headers: {
          Authorization: `Bearer ${tokenGroups}`,
        },
      })
      .then((res) => setSubscriptions(res.data));
  };

  const subscribeToGroup = (group) => {
    setSubscriptions([...subscriptions, group]);
    toast.success("Inscrição realizada com sucesso!");

    api.post(`groups/${group.id}/subscribe/`, null, {
      headers: {
        Authorization: `Bearer ${tokenGroups}`,
      },
    });
  };

  const editGroup = (data, groupId) => {
    subscriptions.map((group) => {
      if (group.id === groupId) {
        group.name = data.name;
        group.description = data.description;
        group.category = data.category;
      }
    });
    toast.success("Informações do grupo editadas com sucesso!");

    api
      .patch(`groups/${groupId}/`, data, {
        headers: {
          Authorization: `Bearer ${tokenGroups}`,
        },
      })
      .then(() => getSubscriptions());
  };

  const createGroup = (name, description, category, userId) => {
    const newGroup = {
      name: name,
      description: description,
      category: category,
      creator: {
        id: userId,
      },
    };
    subscriptions.push(newGroup);
    toast.success("Grupo criado com sucesso!");

    api
      .post("groups/", newGroup, {
        headers: {
          Authorization: `Bearer ${tokenGroups}`,
        },
      })
      .then(() => getSubscriptions());
  };

  const getAllGroups = (page) => {
    api
      .get(`groups/?page=${page}`)
      .then((res) => setAllGroups(res.data.results));
  };

  const getSpecificGroup = (groupName) => {
    const group =
      allGroups.filter((group) =>
        group.name.toLowerCase().includes(groupName.toLowerCase())
      ) || "";
    setSpecificGroup(group);
  };

  return (
    <GroupsContext.Provider
      value={{
        error,
        subscriptions,
        allGroups,
        specificGroup,
        isLoading,
        pages,
        setTokenGroups,
        setSubscriptions,
        setIsLoading,
        setSpecificGroup,
        createGroup,
        subscribeToGroup,
        editGroup,
        getSubscriptions,
        getAllGroups,
        getSpecificGroup,
      }}
    >
      {children}
    </GroupsContext.Provider>
  );
};
