import { HabitsProvider } from "./Habits";
import { GoalsProvider } from "./Goals";
import { ActivitiesProvider } from "./Activities";
import { GroupsProvider } from "./Groups";
import { UserProvider } from "./User";

export const Providers = ({ children }) => {
  return (
    <HabitsProvider>
      <GroupsProvider>
        <GoalsProvider>
          <UserProvider>
            <ActivitiesProvider>{children}</ActivitiesProvider>
          </UserProvider>
        </GoalsProvider>
      </GroupsProvider>
    </HabitsProvider>
  );
};
