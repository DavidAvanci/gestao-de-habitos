import { createContext, useState } from "react";
import { toast } from "react-toastify";
import api from "../../services/api";

export const GoalsContext = createContext();

export const GoalsProvider = ({ children }) => {
  const [specificGoal, setSpecificGoal] = useState({});
  const [groupGoals, setGroupGoals] = useState(undefined);

  const [token] = useState(JSON.parse(localStorage.getItem("@GH:token")) || "");

  const getOneGoal = (goalId) => {
    api.get(`goals/${goalId}`).then((res) => setSpecificGoal(res));
  };

  const getGroupGoals = (id) => {
    api.get(`goals/?group=${id}`).then((res) => {
      setGroupGoals(res.data.results);
    });
  };

  const createGoal = (goal, id) => {
    setGroupGoals([...groupGoals, goal]);
    toast.success("Objetivo criado com sucesso!");

    api
      .post("goals/", goal, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => getGroupGoals(id));
  };

  const updateGoal = (achieved, goalId, groupId) => {
    const updatedInfos = {
      how_much_achieved: achieved,
    };
    toast.success("Objetivo atualizado com sucesso!");

    groupGoals.map((goal) => {
      if (goal.id === goalId) {
        goal.how_much_achieved = achieved;
      }
    });

    api
      .patch(`goals/${goalId}/`, updatedInfos, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => getGroupGoals(groupId));
  };

  const deleteGoal = (goalId, id) => {
    setGroupGoals(groupGoals.filter((goal) => goal.id !== goalId));
    toast.success("Objetivo deletado com sucesso!");

    api
      .delete(`goals/${goalId}/`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => getGroupGoals(id));
  };

  return (
    <GoalsContext.Provider
      value={{
        specificGoal,
        groupGoals,
        getOneGoal,
        getGroupGoals,
        createGoal,
        updateGoal,
        deleteGoal,
      }}
    >
      {children}
    </GoalsContext.Provider>
  );
};
