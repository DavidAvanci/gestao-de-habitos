import { createContext, useState } from "react";
import { toast } from "react-toastify";
import api from "../../services/api";

export const HabitsContext = createContext();

export const HabitsProvider = ({ children }) => {
  const [habitsList, setHabitsList] = useState(undefined);

  const [token, setToken] = useState(
    JSON.parse(localStorage.getItem("@GH:token"))
  );
  const [userId, setUserId] = useState(
    JSON.parse(localStorage.getItem("@GH:userId"))
  );

  const getHabits = () => {
    console.log(token);
    api
      .get("habits/personal/", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => setHabitsList(res.data));
  };

  const createHabit = (habit) => {
    setHabitsList([...habitsList, habit]);
    toast.success("Hábito criado com sucesso!");

    api
      .post("habits/", habit, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => getHabits());
  };

  const updateHabit = (howMuchAchieved, achieved, title, habitId) => {
    habitsList.forEach((item) => {
      if (item.id === habitId) {
        item.title = title;
        item.how_much_achieved = howMuchAchieved;
        item.achieved = achieved;
      }
    });

    const updatedInfos = {
      title: title,
      how_much_achieved: howMuchAchieved,
      achieved: achieved,
    };
    toast.success("Hábito atualizado com sucesso!");

    api
      .patch(`habits/${habitId}/`, updatedInfos, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => getHabits());
  };

  const deleteHabit = (id) => {
    setHabitsList(habitsList.filter((habit) => habit.id !== id));
    toast.success("Hábito deletado com sucesso!");

    api
      .delete(`habits/${id}/`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => getHabits());
  };

  return (
    <HabitsContext.Provider
      value={{
        habitsList,
        token,
        userId,
        setUserId,
        setToken,
        createHabit,
        deleteHabit,
        updateHabit,
        getHabits,
        setHabitsList,
      }}
    >
      {children}
    </HabitsContext.Provider>
  );
};
