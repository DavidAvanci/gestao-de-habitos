import { createContext, useState } from "react";
import { toast } from "react-toastify";
import api from "../../services/api";

export const ActivitiesContext = createContext();

export const ActivitiesProvider = ({ children }) => {
  const [specificActivity, setSpecificActivity] = useState({});
  const [groupActivities, setGroupActivities] = useState(undefined);

  const [token] = useState(JSON.parse(localStorage.getItem("@GH:token")) || "");

  const getOneActivity = (activityId) => {
    api.get(`activities/${activityId}`).then((res) => setSpecificActivity(res));
  };

  const getGroupActivities = (id) => {
    api
      .get(`activities/?group=${id}`)
      .then((res) => setGroupActivities(res.data.results));
  };

  const createActivity = (activity, id) => {
    setGroupActivities([...groupActivities, activity]);
    toast.success("Atividade criada com sucesso!");
    api
      .post("activities/", activity, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => getGroupActivities(id));
  };

  const updateActivity = (title, date, activityId) => {
    const updatedInfos = {
      title: title,
      realization_time: date,
    };
    toast.success("Atividade atualizada com sucesso!")

    groupActivities.map((item) => {
      if (item.id === activityId) {
        item.title = title;
        item.realization_time = date;
      }
    });

    api.patch(`activities/${activityId}/`, updatedInfos, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
  };

  const deleteActivity = (activityId, id) => {
    const newList = groupActivities.filter(
      (activity) => activity.id !== activityId
    );
    setGroupActivities(newList);
    toast.success("Atividade deletada com sucesso!")

    api
      .delete(`activities/${activityId}/`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => getGroupActivities(id));
  };

  return (
    <ActivitiesContext.Provider
      value={{
        specificActivity,
        groupActivities,
        getOneActivity,
        getGroupActivities,
        createActivity,
        updateActivity,
        deleteActivity,
      }}
    >
      {children}
    </ActivitiesContext.Provider>
  );
};
