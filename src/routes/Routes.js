import { Switch, Route } from "react-router-dom";
import { Home } from "../Pages/Home";

import { Login } from "../Pages/Login";
import { Register } from "../Pages/Register";
import { Dashboard } from "../Pages/Dashboard";
import { Groups } from "../Pages/Groups";
import { SpecificGroup } from "../Pages/SpecificGroup";
import { useEffect, useState } from "react";

export const Routes = () => {
  const [authenticated, setAuthenticated] = useState(
    !!JSON.parse(localStorage.getItem("@GH:token"))
  );

  useEffect(() => {
    const token = JSON.parse(localStorage.getItem("@GH:token"));
    if (token) {
      setAuthenticated(true);
    }
  }, [authenticated]);
  return (
    <Switch>
      <Route exact path="/">
        <Home authenticated={authenticated} />
      </Route>
      <Route path="/register">
        <Register authenticated={authenticated} />
      </Route>
      <Route path="/login">
        <Login
          authenticated={authenticated}
          setAuthenticated={setAuthenticated}
        />
      </Route>
      <Route path="/dashboard/:toGo">
        <Dashboard
          authenticated={authenticated}
          setAuthenticated={setAuthenticated}
        />
      </Route>
      {/*       <Route exact path="/groups">
        <Groups
          authenticated={authenticated}
          setAuthenticated={setAuthenticated}
        />
      </Route> */}
      <Route path="/groups/:id">
        <SpecificGroup
          authenticated={authenticated}
          setAuthenticated={setAuthenticated}
        />
      </Route>
    </Switch>
  );
};
