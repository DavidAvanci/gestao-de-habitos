// Import Components
import { Button } from "../Button";
import { Container } from "./style";

export const Modal = ({ children, handleClose, ...rest }) => {
  return (
    <Container className="backZone" {...rest}>
      <div className="modal">
        <button type="button" className="closeModel" onClick={handleClose}>
          X
        </button>
        {children}
      </div>
    </Container>
  );
};
