import styled, { keyframes } from "styled-components";

const appear = keyframes`
from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

export const Container = styled.div`
  animation: ${appear} 0.4s;
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 10;
  .modal {
    pointer-events: initial;
    border-radius: 25px;
    padding: 10px 30px;
    width: 160px;
    background-color: white;
    position: relative;
    .closeModel {
      position: absolute;
      top: 10px;
      right: 15px;
      font-weight: bold;
      font-family: var(--font-text);
      color: red;
    }
  }
`;
