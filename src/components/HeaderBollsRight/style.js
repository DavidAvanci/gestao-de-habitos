import styled from "styled-components";

export const Container = styled.div`
  display: inline-block;
  position: relative;
  width: 70px;
  height: 70px;
  .ball {
    position: absolute;
    background-color: var(--background-color);
  }
  .greatBall {
    width: 32px;
    height: 32px;
    bottom: 15px;
    left: 0;
    border-radius: 100%;
  }
  .middleBall1 {
    width: 20px;
    height: 20px;
    top: 10px;
    right: 15px;
    border-radius: 100%;
  }
  .middleBall2 {
    width: 20px;
    height: 20px;
    bottom: 3px;
    right: 10px;
    border-radius: 100%;
  }
  .smallBall {
    width: 10px;
    height: 10px;
    bottom: 0px;
    left: 15px;
    border-radius: 100%;
  }
`;
