import { Container } from "./style";

export const HeaderBollsRight = () => {
  return (
    <Container>
      <div className="greatBall ball" />
      <div className="middleBall1 ball" />
      <div className="middleBall2 ball" />
      <div className="smallBall ball" />
    </Container>
  );
};
