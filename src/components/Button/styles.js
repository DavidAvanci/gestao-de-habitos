import styled from "styled-components";

export const ButtonContainer = styled.button`
  border-radius: 25px;
  width: 100%;
  max-width: 400px;
  height: 50px;
  padding: 12px;
  background-color: transparent;
  color: var(--purple);
  font-family: var(--font-text);
  border: 2px solid var(--purple);
  text-transform: uppercase;
  font-weight: 550;
  font-size: 20px;
  :hover {
      background-color: var(--purple);
      color: white;
      transition: 0.4s;
  }
`;
