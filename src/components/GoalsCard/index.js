import { Container, ProgressBar, Content, TextContent } from "./styles";
import Pencil from "../../assets/img/pencil.png";
import { FaRegTrashAlt } from "react-icons/fa";

export const GoalsCard = ({
  pencilFunction,
  group,
  isEditable,
  deleteFunction,
  id,
  groupId,
  progress,
  goalTitle,
}) => {
  return (
    <Container>
       <FaRegTrashAlt
        className="Button"
        onClick={() => deleteFunction(id, groupId)}
      />
      <Content>
        <TextContent>
          <p className="goals">Objetivo: {goalTitle}</p>
          <ProgressBar progress={progress}>
            <div className="border">
              <div className="graph"></div>
            </div>
            <p className="progress">{progress}%</p>
          </ProgressBar>
        </TextContent>

        {isEditable && (
          <img
            src={Pencil}
            alt="Editar"
            onClick={() => pencilFunction(group)}
          />
        )}
      </Content>
    </Container>
  );
};
