import styled from "styled-components";

export const Container = styled.div`
  width: 310px;
  height: 110px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  border-radius: 4px;
  background-color: white;
  margin: 6px;

  transition: transform .3s;
  box-shadow: 1px 1px 5px black;

  :hover {
   transform: scale(1.05);
  }

  .Button {
    font-size: 20px;
    border-radius: 5px;
    width: 20px;
    align-self: flex-end;
    margin-right: 5px;
    margin-top: 5px;
    color: red;
    font-weight: bold;
    cursor:pointer;
  }

`;

export const Content = styled.div`
  display: flex;
  flex-direction: row;
  height: 115px;
  max-width: 95%;
  align-items: center;
  justify-content: space-around;

  p {
    font-family: var(--font-text);
  }

  .progress {
    font-size: 13px;
  }

  img {
    background-color: transparent;
    width: 20px;
    cursor: pointer;
  }
`;

export const TextContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 50px;
  justify-content: space-around;

  .goals {
    font-family: var(--font-text);
    font-weight: bold;
  }
`;

export const ProgressBar = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;

  .border {
    border: 1px solid var(--pink);
    width: 100px;
    height: 15px;
  }

  .progress{
    margin-left: 5px;
    font-family: var(--font-text);
  }

  .graph {
    background-color: var(--purple);
    height: 15px;
    width: ${(props) => `${props.progress}%`};
  }
`;
