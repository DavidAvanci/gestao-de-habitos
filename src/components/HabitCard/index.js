import { Container, Circle, Content, ProgressBar} from "./styles";
import Pencil from "../../assets/img/pencil.png";
import { useContext } from "react";
import { HabitsContext } from "../../Providers/Habits";
import { FaRegTrashAlt } from "react-icons/fa";

export const HabitCard = ({ setVisible, habit, setHabitToEdit, title, how_much_achieved }) => {
  const { deleteHabit } = useContext(HabitsContext);
  const handleClick = () => {
    setVisible("edit");
    setHabitToEdit(habit);
  };

  const handleClose = () => {
    deleteHabit(habit.id);
  };

  return (
    <Container>
         <FaRegTrashAlt
        className="closeCard"
        onClick={handleClose}
      />
      <Content>
        <Circle />
        <div className='midleContent'>
          <p>{title}</p>
          <ProgressBar progress={how_much_achieved}>
            <div className="border">
              <div className="graph"></div>
            </div>
            <p className="progress">{how_much_achieved}%</p>
          </ProgressBar>
        </div>
        <img src={Pencil} alt="Editar" onClick={handleClick} />
      </Content>
    </Container>
  );
};
