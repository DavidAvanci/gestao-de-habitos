import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  width: 90%;
  max-width: 286px;
  height: 86px;
  border-radius: 4px;
  background-color: white;
  margin: 10px;

  transition: transform .3s;
  box-shadow: 1px 1px 5px black;

  :hover {
   transform: scale(1.05);
  }


  .closeCard {
    font-weight: bold;
    font-family: var(--font-text);
    align-self: flex-end;
    width: 20px;
    height: 20px;
    margin-right: 10px;
    margin-top: 10px;
    color: red;
    cursor: pointer;
  }

  h3 {
    font-family: var(--font-text);
  }

  img {
    background-color: transparent;
    width: 20px;
    cursor: pointer;
    border: 1px solid transparent;
    padding: 2px;
    :hover {
      border: 1px solid var(--purple);
      border-radius: 8px;
    }
  }

  .children {
    display: flex;
    flex-direction: column;

    p {
      margin: 0;
    }

    button {
      padding: 6px;
      border: none;
      border-radius: 4px;
      background-color: var(--light-blue);
    }
  }
`;

export const Circle = styled.div`
  width: 56px;
  height: 56px;
  border-radius: 25px;
  background-color: var(--purple);
`;

export const Content = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 90px;
  align-items: center;
  justify-content: space-around;

  h3 {
    font-family: var(--font-text);
  }

  img {
    background-color: transparent;
    width: 20px;
    cursor: pointer;
  }
`;

export const ProgressBar = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;

  .border {
    border: 1px solid var(--pink);
    width: 100px;
    height: 15px;
  }

  .progress{
    margin-left: 5px;
    font-family: var(--font-text);
  }

  .graph {
    background-color: var(--purple);
    height: 15px;
    width: ${(props) => `${props.progress}%`};
  }
`;
