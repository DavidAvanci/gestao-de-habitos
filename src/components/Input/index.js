import { InputContainer, Error } from "./styles";

export const Input = ({ name, register, isError, ...rest }) => {
  return (
    <>
      <InputContainer
        name={name}
        isError={!!isError}
        {...register(name)}
        {...rest}
      />
      <Error>{isError?.message || ""}</Error>
    </>
  );
};
