import styled from "styled-components";

export const InputContainer = styled.input`
  border: 1px solid var(--purple);
  border-color: ${({ isError }) => (isError ? "red" : "var(--purple)")};
  border-radius: 5px;
  padding: 12px;
  width: 100%;
  max-width: 400px;
  font-size: 14px;
  outline: none;
  font-family: var(--font-text);
  background-color: rgba(255, 255, 255, 0.75);
  position: relative;
  ::placeholder {
    color: var(--purple);
  }
`;

export const Error = styled.span`
  color: red;
  font-size: 13px;
`;
