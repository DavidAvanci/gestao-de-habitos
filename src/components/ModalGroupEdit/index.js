//import Form
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

// Import Components
import { Button } from "../Button";
import { Input } from "../Input";
import { Modal } from "../Modal";
import { ModalEditGroup } from "./style";

// Import Context
import { GroupsContext } from "../../Providers/Groups";
import { useContext } from "react";

export const EditGroup = ({
  group,
  setModalEditGroup,
  setIsLoading,
  modalEditGroup,
}) => {
  const { editGroup } = useContext(GroupsContext);

  const schema = yup.object().shape({
    name: yup
      .string()
      .required("Nome obrigatório")
      .max(20, "Máximo 20 caracteres"),
    description: yup
      .string()
      .required("Descrição obrigatória")
      .max(20, "Máximo 20 caracteres"),
    category: yup
      .string()
      .required("Categoria obrigatória")
      .max(20, "Máximo 20 caracteres"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (data, e) => {
    console.log(data);
    // setIsLoading(true);
    editGroup(data, group.id);
    setModalEditGroup("none");
    e.target.reset();
  };

  return (
    <ModalEditGroup modalEditGroup={modalEditGroup}>
      <Modal
        handleClose={() => {
          setModalEditGroup("none");
        }}
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <h1>Nome atual: {group.name}</h1>
          <Input
            name="name"
            placeholder="Digite o novo nome"
            register={register}
            isError={errors.name}
          />
          <h1>Descrição atual: {group.description}</h1>
          <Input
            name="description"
            placeholder="Digite a nova descrição"
            register={register}
            isError={errors.description}
          />
          <h1>Categoria atual: {group.category}</h1>
          <Input
            name="category"
            placeholder="Digite a nova categoria"
            register={register}
            isError={errors.category}
          />
          <Button type="submit">Salvar</Button>
        </form>
      </Modal>
    </ModalEditGroup>
  );
};
