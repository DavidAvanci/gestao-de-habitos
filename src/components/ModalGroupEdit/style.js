import styled from "styled-components";

export const ModalEditGroup = styled.div`
  display: ${(props) => props.modalEditGroup};
  > div {
    display: "flex";
    form {
      display: flex;
      flex-direction: column;
      align-items: center;
      h1 {
        text-align: center;
        margin-top: 1rem;
        font-size: 14px;
        font-weight: bold;
        color: var(--purple);
      }
      button {
        width: 100px;
        margin-top: 1rem;
      }
    }
  }
`;
