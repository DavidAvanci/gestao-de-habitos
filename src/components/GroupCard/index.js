import { Container, Circle } from "./styles";
import Pencil from "../../assets/img/pencil.png";

export const GroupCard = ({
  children,
  pencilFunction,
  group,
  isEditable,
  isLoading,
}) => {
  return (
    <Container>
      <Circle />
      {isLoading === true ? <div>Loading</div> : children}
      {isEditable && (
        <img src={Pencil} alt="Editar" onClick={() => pencilFunction(group)} />
      )}
    </Container>
  );
};
