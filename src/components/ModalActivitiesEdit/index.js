//import Form
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

//import componentes
import { Modal } from "../Modal";
import { Container } from "./style";
import { Input } from "../Input";
import { Button } from "../Button";

// Import Toast
import { toast } from "react-toastify";

// import react
import { useContext, useState } from "react";

// import context
import { ActivitiesContext } from "../../Providers/Activities";

export const ModalActivitiesEdit = ({
  modalEditActivities,
  setModalEditActivities,
  activities,
}) => {
  const { updateActivity } = useContext(ActivitiesContext);

  const formSchema = yup.object().shape({
    title: yup
      .string()
      .required("Nome obrigatório")
      .max(20, "Máximo 20 caracteres"),
    realization_time: yup.string().required("preenchimento obrigatório"),
    time: yup.string().required("preenchimento obrigatório"),
  });

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const editActivity = ({ title, realization_time, time }) => {
    const concat = realization_time + "T" + time + "Z";
    updateActivity(title, concat, activities.id);
    setModalEditActivities("none");
    reset()
  };

  return (
    <Container modalEditActivities={modalEditActivities}>
      <Modal
        handleClose={() => {
          setModalEditActivities("none");
        }}
      >
        <form onSubmit={handleSubmit(editActivity)}>
          <h1>Titulo atual: {activities.title}</h1>
          <Input
            className="input"
            register={register}
            placeholder="Digite um novo título"
            name="title"
            isError={errors.title}
          />
          <Input
            className="input"
            register={register}
            type="date"
            name="realization_time"
          />
          <Input register={register} type="time" name="time" />
          <Button type="submit">Salvar</Button>
        </form>
      </Modal>
    </Container>
  );
};
