import { Container, Circle } from "./styles";
import Add from "../../assets/img/add.png";

export const GroupAddCard = ({
    children,
    pencilFunction,
    group,
    isEditable,
    isLoading,
}) => {
    return (
        <Container>
            <Circle />
            {isLoading === true ? <div>Loading</div> : children}
            {isEditable && (
                <img src={Add} alt="Editar" onClick={() => pencilFunction(group)} />
            )}
        </Container>
    );
};
