import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  width: 286px;
  height: 86px;
  border-radius: 4px;
  background-color: white;
  margin: 6px;

  h3 {
    font-family: var(--font-text);
  }

  img {
    background-color: transparent;
    width: 20px;
    cursor: pointer;
    border: 1px solid transparent;
    padding: 2px;
  }
`;

export const Circle = styled.div`
  width: 56px;
  height: 56px;
  border-radius: 25px;
  background-color: var(--purple);
`;
