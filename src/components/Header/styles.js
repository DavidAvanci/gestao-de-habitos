import styled from "styled-components";

export const ContainerHeader = styled.div`
  width: 100%;
  height: 80px;
  background-color: var(--purple);
  text-align: center;
  vertical-align: middle;
  line-height: 80px;
  font-size: 36px;
  font-family: var(--font-title);

  p {
    color: var(--off-white);
  }
`;

export const ContainerHeaderUser = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 60px;
  background-color: var(--purple);
  font-size: 25px;
  font-family: var(--font-text);
  span {
    color: var(--off-white);
    font-size: 15px;
  }
  .profile {
    display: flex;
    flex-direction: row;
    align-items: center;
  }
  .name {
    display: flex;
    flex-direction: row;
    width: 80px;
    line-height: 20px;
    margin-left: 10px;
  }
  .pointer {
    cursor: pointer;
  }

  @media(min-width: 768px) {
    display: none;
  }
`;

export const ContainerProfile = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  margin-left: 20px;
  margin-right: 20px;
`;
