import {
  ContainerHeader,
  ContainerHeaderUser,
  ContainerProfile,
} from "./styles";
import { FaUserAlt, FaRegEdit, FaSignOutAlt } from "react-icons/fa";
import { useHistory } from "react-router";
import { ModalEditName } from "../ModalEditName";
import api from "../../services/api";
import { useContext, useEffect, useState } from "react";
import { HabitsContext } from "../../Providers/Habits";
import { GroupsContext } from "../../Providers/Groups";
import { UserContext } from "../../Providers/User";

export const Header = ({
  children,
  isUser,
  setVisible,
  visible,
  setAuthenticated,
}) => {
  const [userName, setUserName] = useState();
  const { setToken } = useContext(HabitsContext);
  const { setTokenGroups } = useContext(GroupsContext);
  const userId = JSON.parse(localStorage.getItem("@GH:userId"));

  useEffect(() => {
    api.get(`users/${userId}/`).then((res) => setUserName(res.data.username));
  }, [userId]);

  const history = useHistory();
  const handleEdit = () => {
    setVisible("editname");
  };
  const handleSignOut = () => {
    setToken("");
    setTokenGroups("");
    localStorage.clear();
    setAuthenticated(false);
    history.push("/");
  };

  return (
    <>
      {isUser && (
        <ContainerHeaderUser>
          <ContainerProfile>
            <div className="profile">
              <FaUserAlt color="07B0F2" size={40} className="avatar" />
              <div className="name">
                <span>{userName}</span>
              </div>
              <FaRegEdit
                color="white"
                onClick={handleEdit}
                className="pointer"
              />
            </div>
            <FaSignOutAlt
              color="white"
              onClick={handleSignOut}
              className="pointer"
            />
          </ContainerProfile>
          <ModalEditName
            visible={visible}
            setVisible={setVisible}
            setUserName={setUserName}
          />
        </ContainerHeaderUser>
      )}
      {isUser === false && <ContainerHeader>{children}</ContainerHeader>}
    </>
  );
};
