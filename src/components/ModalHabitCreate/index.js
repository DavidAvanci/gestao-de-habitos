//import Form
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

// Import Components
import { InputModal } from "../InputModal";
import { Button } from "../Button";
import { Container } from "./style";
import { Modal } from "../Modal";
import { Input } from "../Input";

// Import Toast
import { toast } from "react-toastify";

// Import Context
import { useContext } from "react";
import { HabitsContext } from "../../Providers/Habits";

export const ModalHabitCreate = ({ modalVisible, setModalVisible }) => {
  const { createHabit } = useContext(HabitsContext);

  const formSchema = yup.object().shape({
    title: yup
      .string()
      .required("Nome obrigatório")
      .max(20, "Máximo 20 caracteres"),
    category: yup
      .string()
      .required("Categoria obrigatória")
      .max(13, "Máximo 13 caracteres"),
    difficulty: yup
      .string()
      .required("Dificuldade obrigatória")
      .max(13, "Máximo 13 caracteres"),
    frequency: yup
      .string()
      .required("Frequencia obrigatória")
      .max(13, "Máximo 13 caracteres"),
    achieved: yup.boolean(),
    how_much_achieved: yup.number(),
  });

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const onSubmit = (data) => {
    const userId = JSON.parse(localStorage.getItem("@GH:userId"));
    createHabit({ ...data, user: userId });
    reset({
      title: "",
      category: "",
      difficulty: "",
      frequency: "",
      achieved: false,
      how_much_achieved: 50,
    });
    setModalVisible(false);
  };

  const handleClose = () => {
    setModalVisible(false);
  };

  return (
    <Container modalVisible={modalVisible}>
      <Modal handleClose={handleClose}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <h1>Informações do Habito</h1>
          <Input
            className="basicInput"
            placeholder="Nome"
            name="title"
            register={register}
            isError={errors.title}
          />
          <Input
            className="basicInput"
            placeholder="Categoria"
            name="category"
            register={register}
            isError={errors.category}
          />
          <Input
            className="basicInput"
            placeholder="Dificuldade"
            name="difficulty"
            register={register}
            isError={errors.difficulty}
          />
          <Input
            className="basicInput"
            placeholder="Frequencia"
            name="frequency"
            register={register}
            isError={errors.frequency}
          />
          <div className="rangeInput">
            <label for="progress">Progresso</label>
            <InputModal
              id="progress"
              type="range"
              min="0"
              max="100"
              name="how_much_achieved"
              register={register}
            />
          </div>
          <InputModal
            type="checkbox"
            className="checkbox"
            name="achieved"
            register={register}
          />
          <Button type="submit">OK</Button>
        </form>
      </Modal>
    </Container>
  );
};
