import { useContext, useEffect, useState } from "react";
import { HabitsContext } from "../../Providers/Habits";
import { Container, HabitsContainer, AddCard } from "./styles";

import { HabitCard } from "../../components/HabitCard";
import { ModalHabitCreate } from "../../components/ModalHabitCreate";
import { ModalHabitEdit } from "../../components/ModalHabitEdit";
import { CircularProgress } from "@material-ui/core";
import { UserContext } from "../../Providers/User";

export const Habits = () => {
  const { habitsList, getHabits } = useContext(HabitsContext);
  const [visible, setVisible] = useState("none");
  const [habitToEdit, setHabitToEdit] = useState({});

  useEffect(() => {
    getHabits();
  }, []);

  const showModalCreate = () => {
    setVisible("create");
  };

  return (
    <Container>
      <AddCard className="addButton" onClick={showModalCreate}>
        Adicionar hábito
      </AddCard>
      <ModalHabitCreate modalVisible={visible} setModalVisible={setVisible} />
      <ModalHabitEdit
        modalVisible={visible}
        setModalVisible={setVisible}
        habit={habitToEdit}
        setHabit={setHabitToEdit}
      />
      <HabitsContainer>
        {habitsList === undefined ? (
          <CircularProgress />
        ) : habitsList.length === 0 ? (
          <div>Você ainda não possui hábitos</div>
        ) : (
          habitsList.map((habit, idx) => (
            <HabitCard
              key={idx}
              setVisible={setVisible}
              habit={habit}
              setHabitToEdit={setHabitToEdit}
              title={habit.title}
              how_much_achieved={habit.how_much_achieved}
           />
          ))
        )}
      </HabitsContainer>
    </Container>
  );
};
