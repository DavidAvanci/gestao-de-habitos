import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  .links {
    display: flex;
    flex-direction: row;
  }

`;

export const HabitsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  margin-top: 20px;

  @media (min-width: 1100px) {
    flex-direction: row;
    justify-content: center;
    flex-wrap: wrap;
  }
`;

export const NewHabit = styled.div`
  display: flex;
  justify-content: center;
  background-color: white;
  width: 90%;
  height: 86px;
  border-radius: 4px;

  button {
    width: 100%;
    text-transform: uppercase;
    :hover {
      background-color: var(--orange);
      transition: 0.5s;
    }
  }

  input {
    border: 1px solid black;
  }
`;

export const AddCard = styled.button`
  border-radius: 25px;
  width: 80%;
  max-width: 300px;
  height: 50px;
  padding: 12px;
  background-color: transparent;
  color: var(--purple);
  font-family: var(--font-text);
  border: 2px solid var(--purple);
  text-transform: uppercase;
  font-weight: 550;
  font-size: 18px;
  margin-top: 30px;
  :hover {
      background-color: var(--purple);
      color: white;
      transition: 0.4s;
  }
`;