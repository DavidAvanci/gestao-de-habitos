import styled from "styled-components";

export const Container = styled.div`
  > div {
    display: ${({ visible }) => (visible === "editname" ? "flex" : "none")};
    form {
      display: flex;
      flex-direction: column;
      align-items: center;
      h1 {
        text-align: center;
        margin-bottom: 20px;
        font-size: 14px;
        font-weight: bold;
        color: var(--purple);
      }
      button {
        width: 100px;
      }
    }
    .rangeInput {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      align-items: center;
      height: 30px;
      label {
        color: var(--purple);
        margin-bottom: 10px;
      }
    }
  }
`;
