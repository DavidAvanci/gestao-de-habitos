//import Form
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

//import components
import { Button } from "../Button";
import { Modal } from "../Modal";
import { Input } from "../Input";
import api from "../../services/api";
import { Container } from "./style";

// Import Toast
import { toast } from "react-toastify";

export const ModalEditName = ({ visible, setVisible, setUserName }) => {
  const formSchema = yup.object().shape({
    username: yup
      .string()
      .required("Nome obrigatório")
      .max(20, "Máximo 20 caracteres"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const onSubmit = (date) => {
    console.log("chegou");
    const token = JSON.parse(localStorage.getItem("@GH:token"));
    const userId = JSON.parse(localStorage.getItem("@GH:userId"));
    api
      .patch(
        `users/${userId}/`,
        { username: date.username },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((res) => setUserName(res.data.username))
      .catch((err) => console.log(err));
    setVisible("none");
  };

  return (
    <Container visible={visible}>
      <Modal handleClose={() => setVisible("none")}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <h1>Alterar nome de usuário</h1>
          <Input
            register={register}
            name="username"
            isError={errors.username}
          />
          <Button type="submit">Mudar</Button>
        </form>
      </Modal>
    </Container>
  );
};
