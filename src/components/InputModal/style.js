import styled, { css } from "styled-components";
import checkSvg from "../../assets/img/Check.svg";

export const Container = styled.input`
  border-bottom: 1px solid var(--purple);
  padding-left: 5px;
  outline: none;
  margin-bottom: 10px;
  width: 160px;
  ::placeholder {
    font-weight: lighter;
    color: var(--purple);
  }
  ${({ type }) =>
    type === "range" &&
    css`
      padding-left: 0;
      width: 100%;
      height: 1px;
      appearance: none;
      background: var(--purple);
      ::-webkit-slider-thumb {
        appearance: none;
        width: 10px;
        height: 10px;
        border-radius: 100%;
        background: white;
        border: 1px solid var(--purple);
        cursor: pointer;
      }
    `}
  ${({ type }) =>
    type === "checkbox" &&
    css`
      appearance: none;
      margin: 20px auto;
      width: 20px;
      height: 20px;
      border: 1px solid var(--purple);
      border-radius: 100%;
      cursor: pointer;
      position: relative;
      :checked::before {
        content: "";
        display: block;
        position: absolute;
        width: 25px;
        height: 25px;
        bottom: 0;
        left: 0;
        background: url(${checkSvg}) no-repeat;
      }
    `}
`;
