import { Container } from "./style";

export const InputModal = ({ register, name, ...rest }) => {
  return <Container {...register(name)} {...rest} />;
};
