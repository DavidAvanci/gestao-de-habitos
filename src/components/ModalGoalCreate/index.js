//import Form
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

//react
import { useContext, useState } from "react";

//import componentes
import { Input } from "../Input";
import { InputModal } from "../InputModal";
import { Modal } from "../Modal";
import { Container } from "./style";
import { GoalsContext } from "../../Providers/Goals";
import { Button } from "../Button";

//import router-dom
import { useParams } from "react-router-dom";

export const ModalGoalCreate = ({ modalNewGoal, setModalNewGoal }) => {
  const { createGoal } = useContext(GoalsContext);

  let { id } = useParams(); // Parametro que fornece o id do grupo

  const [newGoal, setNewGoal] = useState({
    title: "",
    difficulty: "",
    how_much_achieved: "",
  });

  const { title, difficulty, how_much_achieved } = newGoal;

  const formSchema = yup.object().shape({
    title: yup
      .string()
      .required("Nome obrigatório")
      .max(20, "Máximo 20 caracteres"),
    difficulty: yup
      .string()
      .required("Dificuldade obrigatória")
      .max(20, "Máximo 20 caracteres"),
    how_much_achieved: yup.number(),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const submitGoal = (data) => {
    const newData = {
      ...data,
      group: id,
    };
    createGoal(newData, id); // cria novo objetivo dentro da api
    setModalNewGoal("none"); // Retira visibilidade do modal de criação de novo objetivo
    setNewGoal({ title: "", difficulty: "", how_much_achieved: "" }); // Limpar os inputs
  };

  return (
    <Container modalNewGoal={modalNewGoal}>
      <Modal
        className="modal"
        handleClose={() => {
          setModalNewGoal("none");
        }}
      >
        <form onSubmit={handleSubmit(submitGoal)}>
          <h1>Criar novo Objetivo</h1>
          <Input
            register={register}
            value={title}
            onChange={(e) => setNewGoal({ ...newGoal, title: e.target.value })}
            placeholder="Titulo"
            name="title"
            isError={errors.title}
          />
          <Input
            name="difficulty"
            register={register}
            value={difficulty}
            onChange={(e) =>
              setNewGoal({ ...newGoal, difficulty: e.target.value })
            }
            placeholder="Dificuldade"
            isError={errors.difficulty}
          />
          <div className="rangeInput">
            <label for="progress">Progresso</label>
            <InputModal
              id="progress"
              name="how_much_achieved"
              register={register}
              value={how_much_achieved}
              type="range"
              onChange={(e) =>
                setNewGoal({ ...newGoal, how_much_achieved: e.target.value })
              }
            />
          </div>
          <Button type="submit">Criar</Button>
        </form>
      </Modal>
    </Container>
  );
};
