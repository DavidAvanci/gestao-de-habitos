import styled from "styled-components";

export const Container = styled.div`
  > div {
    display: ${({ modalNewGoal }) => modalNewGoal};
    form {
      display: flex;
      flex-direction: column;
      align-items: center;
      h1 {
        text-align: center;
        margin-bottom: 20px;
        font-size: 14px;
        font-weight: bold;
        color: var(--purple);
      }
      input {
        margin-bottom: 10px;
      }
      button {
        width: 90px;
        margin-top: 1rem;
      }
    }
    .rangeInput {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      align-items: center;
      height: 30px;
      label {
        color: var(--purple);
        margin-bottom: 10px;
      }
    }
  }
`;
