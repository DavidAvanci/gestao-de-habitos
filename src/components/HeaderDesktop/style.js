import styled from "styled-components";

export const Container = styled.div`
  display: none;
  @media (min-width: 768px) {
    display: initial;
    .userAvatar {
      position: absolute;
      top: 50px;
      left: 50px;
      display: flex;
      align-items: center;
      width: 165px;
      justify-content: space-evenly;

      span {
        color: var(--purple);
        font-size: 20px;
        font-weight:bold;
      }
    }

    .signOut {
      position: absolute;
      top: 50px;
      right: 50px;
      display: flex;
      align-items: center;
      width: 165px;
      justify-content: space-evenly;
      cursor:pointer;
    }
  }
`;
