import { FaSignOutAlt, FaUserAlt } from "react-icons/fa";
import { Container } from "./style";
import { ModalEditName } from "../ModalEditName";
import { FiSettings } from "react-icons/fi";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import api from "../../services/api";

export const HeaderDesktop = ({ visible, setVisible, setAuthenticated}) => {
  const [userName, setUserName] = useState();
  const userId = JSON.parse(localStorage.getItem("@GH:userId"));
  useEffect(() => {
    api.get(`users/${userId}/`).then((res) => setUserName(res.data.username));
  }, [userId]);

  const history = useHistory();
  const handleEdit = () => {
    setVisible("editname");
  };
  const handleSignOut = () => {
    localStorage.clear();
    setAuthenticated(false);
    history.push("/");
  };

  return (
    <>
      <Container>
        <div className="userAvatar">
          <FaUserAlt color="07B0F2" size={40} />
          <span>{userName}</span>
        </div>
        <div className="signOut">
          <FiSettings color="var(--purple)" size={40} onClick={handleEdit}/>
          <FaSignOutAlt
            color="var(--purple)"
            size={40}
            onClick={handleSignOut}
          />
        </div>
      </Container>
      <ModalEditName
        visible={visible}
        setVisible={setVisible}
        setUserName={setUserName}
      />
    </>
  );
};
