import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  display: inline-block;
  margin-top: 0.5rem;
  img {
    position: absolute;
    left: -38px;
    bottom: 25px;
    width: ${({ desktop }) => (desktop ? "70px" : "60px")};
    height: ${({ desktop }) => (desktop ? "70px" : "60px")};
  }
  p {
    font-family: var(--font-title);
    color: ${({ desktop }) =>
      desktop ? "var(--purple)" : "var(--background-color)"};
    display: inline;
    font-size: ${({ desktop }) => (desktop ? "48px" : "36px")};
  }
`;
