import logo from "../../assets/img/Logo.svg";
import logo2 from "../../assets/img/Logo2.svg";
import { Container } from "./style";

export const Logo = ({ desktop }) => {
  return (
    <Container desktop={desktop}>
      <img alt="logomarca" src={desktop ? logo2 : logo} />
      <p>Fit++</p>
    </Container>
  );
};
