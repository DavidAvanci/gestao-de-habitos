import { Container, Circle, Content, TextContent } from "./styles";
import Pencil from "../../assets/img/pencil.png";
import { FaRegTrashAlt } from "react-icons/fa";
export const ActivityCard = ({
  date,
  activitiesTitle,
  pencilFunction,
  group,
  isEditable,
  deleteFunction,
  id,
  groupId,
}) => {
  return (
    <Container>
      <FaRegTrashAlt
        className="Button"
        onClick={() => deleteFunction(id, groupId)}
      />
      <Content>
        <Circle />
        <TextContent>
          <p className="title">{activitiesTitle}</p>
          <p className="date">
            {new Date(date).toLocaleDateString("pt-BR", {
              day: "2-digit",
              month: "long",
              year: "numeric",
            })}
          </p>
          <p className="date">
            {new Date(date).toLocaleTimeString([], {
              timeZone: "UTC",
              hour: "2-digit",
              minute: "2-digit",
            })}
          </p>
        </TextContent>
        {isEditable && (
          <img
            src={Pencil}
            alt="Editar"
            onClick={() => pencilFunction(group)}
          />
        )}
      </Content>
    </Container>
  );
};
