import styled from "styled-components";

export const Container = styled.div`
  width: 310px;
  height: 115px;
  border-radius: 4px;
  background-color: white;
  margin: 6px;
  transition: transform .3s;
  box-shadow: 1px 1px 5px black;

  :hover {
   transform: scale(1.05);
  }

  .Button {
    font-size: 20px;
    width: 20px;
    padding-left: 275px;
    padding-top: 10px;
    align-self: flex-end;
    color: red;
    font-weight: bold;
  }
`;

export const Content = styled.div`
  display: flex;
  flex-direction: row;
  height: 60px;
  align-items: center;
  justify-content: space-around;

  h3 {
    font-family: var(--font-text);
  }

  img {
    background-color: transparent;
    width: 20px;
    cursor: pointer;
  }
`;

export const TextContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 50px;
  justify-content: space-around;

  .title {
    color: var(--purple);
    font-weight: bold;
  }
  .date {
    color: black;
  }
`;

export const Circle = styled.div`
  width: 56px;
  height: 56px;
  border-radius: 25px;
  background-color: var(--purple);
`;
