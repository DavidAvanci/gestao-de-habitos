//import Form
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

// Import API baseUrl
import api from "../../services/api";

// Import Components
import { InputModal } from "../InputModal";
import { Button } from "../Button";
import { Container } from "./style";
import { Modal } from "../Modal";

// Import Toast
import { toast } from "react-toastify";

// Import Context
import { useContext, useEffect } from "react";
import { HabitsContext } from "../../Providers/Habits";

export const ModalHabitEdit = ({
  modalVisible,
  setModalVisible,
  habit,
  setHabit,
}) => {
  const { updateHabit } = useContext(HabitsContext);

  const formSchema = yup.object().shape({
    achieved: yup.boolean(),
    how_much_achieved: yup.number(),
  });

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const onSubmit = (habit) => {
    updateHabit(habit.how_much_achieved, habit.achieved, habit.title, habit.id);
    setModalVisible("none");
  };

  const handleClose = () => {
    setModalVisible("none");
  };

  useEffect(() => {
    reset(habit);
  }, [reset, habit]);

  return (
    <Container modalVisible={modalVisible}>
      <Modal handleClose={(e) => handleClose(e)}>
        <form autocomplete="off" onSubmit={handleSubmit(onSubmit)}>
          <h1>Alterar Habito: {habit.title}</h1>
          <div className="rangeInput">
            <label for="progress">Progresso</label>
            <InputModal
              id="progress"
              type="range"
              min="0"
              max="100"
              name="how_much_achieved"
              register={register}
              value={habit.how_much_achieved}
              onChange={(e) => {
                setHabit({
                  ...habit,
                  how_much_achieved: e.target.value,
                });
              }}
            />
          </div>
          <InputModal
            type="checkbox"
            className="checkbox"
            name="achieved"
            register={register}
            checked={habit.achieved}
            onChange={(e) => {
              setHabit({ ...habit, achieved: e.target.checked });
            }}
          />
          <Button type="submit">OK</Button>
        </form>
      </Modal>
    </Container>
  );
};
