//import Form
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

//import componentes
import { InputModal } from "../InputModal";
import { Modal } from "../Modal";
import { Container } from "./style";
import { Button } from "../Button";

// Import Toast
import { toast } from "react-toastify";

//import react
import { useContext, useEffect } from "react";

// Import Context
import { GoalsContext } from "../../Providers/Goals";

// Import from React Router
import { useParams } from "react-router";

export const ModalGoalEdit = ({ modalEditGoals, setModalEditGoals, goal }) => {
  const { updateGoal } = useContext(GoalsContext);

  let { id } = useParams(); // Parametro que fornece o id do grupo

  const formSchema = yup.object().shape({
    how_much_achieved: yup.number(),
  });

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  // Função acionada quando o usuário salva novas alteraçõe feitas no objetivo
  // pelo usuário;
  const editGoal = (achieved) => {
    setModalEditGoals("none"); // Tira a visibilidade do modal edit
    updateGoal(achieved.how_much_achieved, goal.id, id); //atualiza o goal na api
  };

  const handleClose = () => {
    setModalEditGoals("none");
  };

  useEffect(() => {
    reset({ how_much_achieved: goal.how_much_achieved });
  }, [goal, reset]);

  return (
    <Container modalEditGoals={modalEditGoals}>
      <Modal handleClose={handleClose}>
        <form onSubmit={handleSubmit(editGoal)}>
          <h1>Modificar Progresso</h1>
          <div className="rangeInput">
            <label for="progress">Progresso</label>
            <InputModal
              id="progress"
              type="range"
              min="0"
              max="100"
              name="how_much_achieved"
              register={register}
            />
          </div>
          <span className="percentage">Atual: {goal.how_much_achieved}%</span>
          <Button type="submit">Ok</Button>
        </form>
      </Modal>
    </Container>
  );
};
