import styled from "styled-components";

export const Container = styled.div`
  > div {
    display: ${({ modalEditGoals }) => modalEditGoals};
    form {
      display: flex;
      flex-direction: column;
      align-items: center;
      h1 {
        text-align: center;
        margin-bottom: 20px;
        font-size: 14px;
        font-weight: bold;
        color: var(--purple);
      }
      button {
        width: 90px;
        margin-top: 1rem;
      }
    }
    .rangeInput {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      align-items: center;
      height: 30px;
      margin-bottom: 1rem;
      label {
        color: var(--purple);
        margin-bottom: 10px;
      }
    }
    .percentage {
      color: var(--purple);
      font-weight: lighter;
    }
  }
`;
