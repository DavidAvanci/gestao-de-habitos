import React from "react";
import {Container} from './style'

export const PreviousPage = ({previousPage}) => {
  return (
    <Container onClick={previousPage} />
  );
};
