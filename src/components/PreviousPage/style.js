import styled from "styled-components";
import Icon from "../../assets/img/previouPage.svg";

export const Container = styled.div`
  background: url(${Icon}) no-repeat center;
  width: 30px;
  height: 30px;
  padding: 5px;
  cursor:pointer;
`;
