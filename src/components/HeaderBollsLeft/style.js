import styled from "styled-components";

export const Container = styled.div`
  display: inline-block;
  position: relative;
  width: 70px;
  height: 70px;
  .ball {
    position: absolute;
    background-color: var(--light-blue);
  }
  .greatBall {
    width: 32px;
    height: 32px;
    top: 0;
    left: 0;
    border-radius: 100%;
  }
  .middleBall1 {
    width: 20px;
    height: 20px;
    bottom: 10px;
    left: 0;
    border-radius: 100%;
  }
  .middleBall2 {
    width: 20px;
    height: 20px;
    top: 30px;
    right: 15px;
    border-radius: 100%;
  }
  .smallBall {
    width: 10px;
    height: 10px;
    bottom: 3px;
    right: 30px;
    border-radius: 100%;
  }
`;
