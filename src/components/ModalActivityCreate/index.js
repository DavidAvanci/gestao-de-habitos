//import Form
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

//import components
import { Input } from "../Input";
import { Modal } from "../Modal";
import { Container } from "./style";
import { Button } from "../Button";

// Import Toast
import { toast } from "react-toastify";

// import react
import { useContext, useState } from "react";

// import context
import { ActivitiesContext } from "../../Providers/Activities";

export const ModalActivityCreate = ({
  modalNewActivity,
  setModalNewActivity,
  id,
  setActivities,
}) => {
  const { createActivity } = useContext(ActivitiesContext);

  const formSchema = yup.object().shape({
    title: yup
      .string()
      .required("Título obrigatório")
      .max(20, "Máximo 20 caracteres"),
    realization_time: yup.string().required("preenchimento obrigatório"),
    time: yup.string().required("preenchimento obrigatório"),
  });

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const submitActivity = ({ title, realization_time, time }) => {
    reset();
    const concat = realization_time + "T" + time + "Z";
    createActivity(
      {
        title: title,
        realization_time: concat,
        group: parseInt(id),
      },
      id
    );
    setModalNewActivity("none");
    setActivities("");
  };

  return (
    <Container modalNewActivity={modalNewActivity}>
      <Modal
        handleClose={() => {
          setModalNewActivity("none");
        }}
      >
        <form onSubmit={handleSubmit(submitActivity)}>
          <h1>Criar atividade</h1>
          <Input
            className="input"
            register={register}
            placeholder="Nova atividade"
            name="title"
            isError={errors.title}
          />
          <Input
            className="input"
            register={register}
            type="date"
            name="realization_time"
          />
          <Input register={register} type="time" name="time" />
          <Button type="submit">Criar</Button>
        </form>
      </Modal>
    </Container>
  );
};
