import styled from "styled-components";

export const Container = styled.div`
  > div {
    display: ${({ modalNewActivity }) => modalNewActivity};
    .input{
      margin-bottom: 10px;
    }
    form {
      display: flex;
      flex-direction: column;
      align-items: center;
      h1 {
        text-align: center;
        margin-bottom: 20px;
        font-size: 14px;
        font-weight: bold;
        color: var(--purple);
      }
      button {
        width: 100px;
        margin-top: 1rem;
      }
    }
  }
`;
