import styled from "styled-components";

export const ModalNewGroup = styled.div`
  display: ${(props) => props.modalNewGroup};
  > div {
    display: "flex";
    form {
      display: flex;
      flex-direction: column;
      align-items: center;
      h1 {
        text-align: center;
        margin-bottom: 20px;
        font-size: 14px;
        font-weight: bold;
        color: var(--purple);
      }
      input {
        margin-top: 10px;
      }
      button {
        margin-top: 10px;
        width: 90px;
      }
    }
  }
`;
