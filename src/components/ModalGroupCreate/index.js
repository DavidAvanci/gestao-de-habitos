//import Form
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

// Import Components
import { Button } from "../Button";
import { Input } from "../Input";
import { Modal } from "../Modal";
import { ModalNewGroup } from "./style";

// Import Context
import { GroupsContext } from "../../Providers/Groups";
import { useContext } from "react";

export const NewGroup = ({ setModalNewGroup, userId, modalNewGroup }) => {
  const { createGroup } = useContext(GroupsContext);

  const schema = yup.object().shape({
    name: yup
      .string()
      .required("Campo obrigatório")
      .max(20, "Máximo 20 caracteres"),
    description: yup
      .string()
      .required("Campo obrigatório")
      .max(20, "Máximo 20 caracteres"),
    category: yup
      .string()
      .required("Campo obrigatório")
      .max(20, "Máximo 20 caracteres"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (data, e) => {
    console.log(data);
    const { name, description, category } = data;
    createGroup(name, description, category, userId);
    setModalNewGroup("none");
    e.target.reset();
  };

  return (
    <ModalNewGroup modalNewGroup={modalNewGroup}>
      <Modal
        handleClose={() => {
          setModalNewGroup("none");
        }}
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <h1>Criar grupo</h1>
          <Input
            register={register}
            placeholder="Nome do Grupo"
            name="name"
            isError={errors.name}
          />
          <Input
            name="description"
            placeholder="Digite a nova descrição"
            register={register}
            isError={errors.description}
          />
          <Input
            name="category"
            placeholder="Digite a nova categoria"
            register={register}
            isError={errors.category}
          />
          <Button type="submit">Criar</Button>
        </form>
      </Modal>
    </ModalNewGroup>
  );
};
